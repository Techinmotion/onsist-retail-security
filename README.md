# Preparing For E-Commerce Success: How To Ensure Your E-Commerce Website Will Work #

Despite consumers' familiarity with purchasing products from the Internet, and the prevalence of website developers eager to work on e-commerce projects, developing a successful e-commerce website is no trivial matter.

Certain key factors must be considered before your website is built. Failing to consider these issues first invariably results in failure. Use the following 7 points to check whether your business is ready to benefit from e-commerce.

1. How Suitable Are My Products For An E-Commerce Website?

Not everything is suited to online selling and so it is vital that you understand whether consumers are likely to buy your products directly from a website. Suitable products tend to share common features:

Homogenous - branded products are identical, irrespective of where they are purchased, and these are often good candidates for an e-commerce website. A consumer is more likely to purchase an item from your website if they understand exactly what they are buying. Books are a good example: a novel bought from one website is identical to the same novel bought from any other and so, in the consumer's mind, there is no uncertainty about what they are purchasing. Conversely, personalised or unique items (or products that need to be tried on or tested) are not so well suited to online selling. Shoes and spectacles, for instance, are difficult to sell from an e-commerce website because the consumer is more uncertain about whether the item is exactly right for them.
Shippable - the fulfilment of online orders is often the area of e-commerce that causes website owners most difficulty. Small, light-weight items are easily shipped and do not involve a high delivery cost that may deter online sales. To this end, many website owners will incorporate the actual delivery cost within the price of their products so that they are able to offer "free delivery", which can help to stimulate online orders. Bulky, heavy items can pose website owners some problems when it comes to delivery. If a product is fairly low-value item, will a £10 or £20 additional delivery charge be acceptable to the consumer?
Inexpensive - consumers' propensity to purchase online is strongly related to the perceived risk of making that purchase. A normally cautious consumer might adopt a more care-free "why not?" attitude when purchasing a book for £6.99. The same person would be far more reticent when spending several thousand pounds on an electrical product. Although high-price products do sell on the Internet, you have a natural advantage if you sell lower value items.
2. What Is Your Target Market?
Any Internet enthusiast will tell you that you can sell to customers anywhere in the world as soon as you have an e-commerce website.

If you run a conventional business that supplies to customers in a specific geographic area, the prospect of being able to sell across the world can be very enticing.

However, fulfilling an overseas order can be quite onerous: should you charge VAT to customers based outside the EU? Do you have to pay excise duties if you send an order to America? How much will it cost to deliver an order to New Zealand?

With this in mind, many business owners choose to restrict their website to accepting orders from the UK only, particularly while their e-commerce site is in its early stages. The downside of selling to a more confined market can sometimes be a sensible price to pay.

3. Payment Processing - Real-time or Offline?

Many websites complete online transactions at the exact time of purchase, while the customer is still viewing the website. This is real-time processing and it uses a Payment Service Provider (PSP) - an online service that provides:

A secure environment in which the customer can safely enter credit / debit card details;
An automated connection to the banking system, so that the card transaction can be verified and checked;
Secure Trading and WorldPay are two well-known PSPs.
Alternatively, off-line processing means that your website will simply collect the customer's card details and store them securely on the server. The transaction is processed manually at a later date, usually by the website owner using a PDQ machine (credit card swipe machine).

Real-time processing requires no intervention on your behalf. As the website owner, you will be notified when a transaction is processed (although you will not see the customer's card details) and you will then fulfil the order accordingly. Because this is an automated solution, it is hassle-free although it can be more costly, as your bank and the PSP will both take a percentage of the value of each transaction.

Off-line processing is dependent on your involvement, which can prove time consuming but it does provide you with the opportunity to vet customers' orders before you accept their payments.

4. How Will You Attract Customers To Your Website?

If you run a conventional retail outlet, you will be familiar with the adage that the three most important factors in retailing are "location, location, location". Retailers know that a good location, with a steady stream of passing traffic, guarantees a high level of footfall.

Passing traffic is equally important for an e-commerce website. The most beautifully-designed, functional site will not generate revenue from online sales unless it receives visitors. An effective, well-planned approach to driving traffic to your website is a vital component of a successful e-commerce site.

Your immediate thoughts about generating visitors may involve Internet-based marketing. A prominent listing in Google, Yahoo and other leading search engines will invariably produce visits, so a well-optimised website may well be important. Similarly, a marketing campaign based on Google AdWords(TM) can also direct prospective customers to your website.

However, just because your shop exists online, you should not overlook off-line marketing as an effective means of generating visitors. Press releases, word-of-mouth referrals, magazine advertising and PR all have a part to play in your overall marketing strategy.

Many website owners focus too much on their site's functionality and "look and feel" and then find that it is the lack of traffic that causes their website to fail. Understanding how you will generate visitors to your website is the most important factor in your e-commerce plans.

5. Do You Understand The Legal Issues?

Using your website to trade online introduces legal obligations that may not apply if you have a brochure-style website.

E-commerce website owners cannot trade anonymously. Your e-commerce site should show your contact details, including a full postal address, as well as details of the legal owner of the site.

You should also be aware that customers have the legal right to return a product bought from your website within 7-days of delivery. A customer does not need to provide a reason and you cannot refuse to accept the returned item (unless your products are exempt, as is the case with food retailers and businesses that sell personalised items).

Data protection, copyright and accessibility are other key legal issues that apply to online trading and you should consult the DTI's website or talk to a solicitor to get suitable advice.

6. How Will You Choose an Appropriate Website Designer?

Website designers broadly fall into one of three categories:

Graphic design firms offer print-based design services in addition to designing websites
Multimedia artists create sophisticated design work, often involving 3D animation and sound
Web developers have more of an IT bias and their technical skills are well-suited to building web applications as well as website design
E-commerce websites are particularly suited to web developers, as their technical expertise means that they are well-positioned to build secure, dynamic websites.
Choosing a new website designer can be difficult so ensure that you check the following before you make your choice:

Have they developed e-commerce websites before? Ask for addresses of other clients' websites and visit each site to check that it works well
Are they knowledgeable about key e-commerce issues such as payment processing and website marketing? Ask them to explain how they would approach this for your website
Do they follow a structured approach to their work or do things appear somewhat haphazard? A successful e-commerce website will require careful planning and you do not want to suffer just because your website designer is anxious to get the job done
7. Will Your Website Be Sufficiently Flexible?
It is important to check that your website will include e-commerce features that give you sufficient flexibility and control:

Product updates - ideally, you want to be in control of your website once it is developed, so you will need the facility to update product details as and when you wish, without needing involvement of your website designer
Stock control - retailers and distributors who can source products with a short lead-time can use their website to sell unlimited stock. Other businesses may need their website to enforce stock control, which allows the website owner to specify how many items are available at any time
Discounts and offers - you may wish to apply a discount to certain products or place an item on special offer. Buy-one-get-one-free, x% discount and free delivery are typical special offers that you can apply
Selling overseas - if you are selling outside the UK, how well will your website cater for overseas customers? Can you show product prices in different currencies, for example? Can you apply a different delivery charge depending on whether the customer is buying from the UK, Europe or the rest of the world?
Whether your e-commerce site is a new website or an upgrade to your existing brochure-style site, it is a major step for your business. The details involved are complex and the whole process can feel overwhelming. However, a successful online presence can produce a profitable new revenue stream, which will help you to grow your business and set you apart from your competitors.

* [https://www.onsist.com/retail-security/](https://www.onsist.com/retail-security/)

